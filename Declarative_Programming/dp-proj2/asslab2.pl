correspond(E1, [E1|_], E2, [E2|_]).
correspond(E1, [_|T1], E2, [_|T2]):-
    correspond(E1, T1, E2, T2).

head([X|_], X).
interleave([[]], []).
%% Deal with [[],[],[]...]
interleave([[]|A], []) :-
    head(A, []),
    interleave(A, []).
interleave([[A|B]|C], [A|Hs]) :-
    append(C,[B],R1),
    interleave(R1, Hs).

%% ----- version 2 ------
%% given a list of lists, extract all heads of lists into an a new list, 
%% put all tails into another new list.
%% splithead(Ls, [heads], [tails])
splithead([],[],[]).
splithead([[H|T]|Rst1],[H|Rst2],[T|Rst3]) :-
    splithead(Rst1, Rst2, Rst3).

split(N,L,L1,L2):-
    append(L1,L2,L),
    length(L1,N).

interleave1([[]],[]).
interleave1([[]|T],[]):-
    interleave1(T, []).
interleave1(Ls, Rs):-
    splithead(Ls, Hs, Ts),
    length(Ls, N),
    N > 0,
    split(N, Rs, Hs, R2),
    interleave1(Ts, R2).

interleave2([[],[]],[]).
interleave2(Ls, L):-
    splithead(Ls, Head, Tail),
    append(Head, L1, L),
    interleave2(Tail, L1).

