-- |
-- Module      :  Pitch
-- Author 	   :  Heguang Miao
-- Login Name  :  hmiao 718712
-- Purpose     :  Defines a bunch of data types.
-- 
-- This file defines basic data types and inplements type class methods.
module Pitch where

import Data.List

data Octave = O1 | O2 | O3 
    deriving(Eq, Ord, Enum, Bounded)

data Note = A | B | C | D | E | F | G
    deriving(Eq, Ord, Enum, Bounded, Show, Read)

data Pitch = Pitch { note :: Note, octave :: Octave}
    deriving (Eq)

octavechars :: String
octavechars = "123"

instance Show Octave where
    show o = [octavechars !! fromEnum o]

instance Show Pitch where
    show (Pitch n o) = show n ++ show o

instance Read Octave where
    readsPrec _ str
        | str == [] = []
        | otherwise = 
            case elemIndex (head str) octavechars of
                Nothing -> []
                Just i -> [(toEnum i, tail str)] 

instance Read Pitch where
    readsPrec _ str = 
        [(Pitch n o, drop 2 str) | n <- [read $ take 1 str :: Note], o <- [read $ take 1 (drop 1 str) :: Octave]]
