-- |
-- Module      :  PitchGuess
-- Author 	   :  Heguang Miao 718712
-- Login Name  :  hmiao
-- Purpose     :  The guessing process
-- 
-- This file contains the main guessing strategy.

module PitchGuess(initialGuess,nextGuess) where

import Common
import qualified Data.Set as Set

{- |
 - InitialGuess takes no arguments and returns the first guess and the initial game state
 - The initial game state contains all possible answers.
-}
initialGuess :: ([String], GameState)
initialGuess = (["A1", "D2", "G3"], GameState 1 $ [Pitch n o | n <- [A .. G], o <- [O1 .. O3]] `choose` 3) 

{- |
 - `rateGuessWithOptions` rates a guess by the number of possible feedbacks in the next step.
 - The first argument is a guess and the second is the remained possible options.
 - The guess with the smallest rate is the best one, because it can filter out more options
 - in the next guess.
-}
rateGuessWithOptions :: Selection -> [Selection] -> Int 
rateGuessWithOptions guess options= 
    let groupedByFeedback = Set.fromList [response guess ans | ans <- options]
    in Set.size groupedByFeedback

{- |
 - This function takes a list of candidates as the argument and find out the best guess
 - by rates.
-}
findBestGuess :: [Selection] -> Selection
findBestGuess [] = []
findBestGuess options = 
    let 
        rates = [(rateGuessWithOptions ans options, ans) | ans <- options]
        betterSelection s1@(rate1, _) s2@(rate2, _) =
            if rate1 <= rate2 then s2 else s1
        (_, res) = foldl1 betterSelection rates
    in res 

{- |
 - This function filters out impossible answers.
 - The first argument is the previous guess
 - The second argument is the feedback of the previous guess
 - The third arguments is a list of remained answers.
-}
filterOptions :: Selection -> Feedback -> [Selection] -> [Selection]
filterOptions prevGuess feedback = 
    filter (\ans -> response ans prevGuess == feedback)

{-|
 - Given the previous guess, its feedback and game state, returns next guess.
-}
nextGuess :: ([String], GameState) -> Feedback -> ([String], GameState)
nextGuess (prevGuess, GameState no prevOptions) feedback = 
    let
        prevGuess' = convertStringsToPitches prevGuess -- convert to Pitch type
        options = filterOptions prevGuess' feedback prevOptions
    in (convertPitchesToStrings $ findBestGuess options, GameState(no+1) options)
