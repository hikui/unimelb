-- |
-- Module      :  Common
-- Author 	   :  Heguang Miao
-- Login Name  :  hmiao
-- Purpose     :  This file includes helpers and additional data types.
--


module Common (module Pitch, module Common) where

import Pitch
import Data.List

-- aliases
type Selection = [Pitch]
type Feedback = (Int, Int, Int)

data GameState = GameState Int [Selection]

instance Show GameState where
    show (GameState i xs) = "GameState(" ++ show i ++ ", " ++ show (length xs) ++ ")"

{- | 
    Get feedback from a guess. 
    It takes two arguments, the first is a guess and the second is the target.
    The feedback includes the number of right pitches, the number of right notes
    and the number of right octaves.
-}
response :: [Pitch] -> [Pitch] -> Feedback
response [] _ = (0, 0, 0)
response guess target = (right, rightNote, rightOctave)
    where 
        right = length $ intersect guess target
        num = length guess
        rightNote = num - (length $ deleteFirstsBy (\ (Pitch gn _) (Pitch tn _) -> gn == tn) guess target) - right
        rightOctave = num - (length $ deleteFirstsBy (\ (Pitch _ go) (Pitch _ to) -> go == to) guess target) - right


{- |
    Given a list, returns its subsets of length n.
-}
choose :: [b] -> Int -> [[b]]
_ `choose` 0 = [[]]
[] `choose` _  = []
(x:xs) `choose` k = (x:) `fmap` (xs `choose` (k-1)) ++ xs `choose` k


-- helpers
convertStringsToPitches :: [String] -> [Pitch]
convertStringsToPitches [] = []
convertStringsToPitches (s:ss) = [(read s :: Pitch)] ++ convertStringsToPitches ss

convertPitchesToStrings :: [Pitch] -> [String]
convertPitchesToStrings [] = []
convertPitchesToStrings (p:ps) = [(show p)] ++ convertPitchesToStrings ps



