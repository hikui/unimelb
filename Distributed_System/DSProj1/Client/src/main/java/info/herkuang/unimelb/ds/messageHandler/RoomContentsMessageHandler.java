package info.herkuang.unimelb.ds.messageHandler;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class RoomContentsMessageHandler implements MessageHandler{
    @Override
    public void handleMessage(String message) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String owner = (String) obj.get("owner");
        String roomId = (String) obj.get("roomid");
        System.out.print(roomId + " contains");
        JSONArray identities = (JSONArray) obj.get("identities");
        for (Object o : identities) {
            String identity = (String)o;
            if (owner != null && identity.equals(owner)){
                identity += "*";
            }
            System.out.print(" " + identity);
        }
        System.out.print('\n');
    }
}
