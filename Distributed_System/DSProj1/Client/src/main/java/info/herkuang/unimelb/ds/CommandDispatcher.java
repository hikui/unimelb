package info.herkuang.unimelb.ds;

import info.herkuang.unimelb.ds.commandHandler.*;
import info.herkuang.unimelb.ds.exceptions.InvalidCommandException;
import info.herkuang.unimelb.ds.util.CommonUtil;
import info.herkuang.unimelb.ds.util.JSONMessageComposer;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class CommandDispatcher {
    public static void dispatch(String command, ChatClient client) {
        try {
            if (command.startsWith("#identitychange ")) {
                new IdentityChangeCommandHandler().handleCommand(command, client);
            } else if (command.startsWith("#who")){
                new WhoCommandHandler().handleCommand(command, client);
            } else if (command.startsWith("#list")) {
                new ListCommandHandler().handleCommand(command, client);
            } else if (command.startsWith("#createroom ")) {
                new CreateRoomCommandHandler().handleCommand(command, client);
            } else if (command.startsWith("#join ")){
                new JoinCommandHandler().handleCommand(command, client);
            } else if (command.startsWith("#kick ")) {
                new KickCommandHandler().handleCommand(command, client);
            } else if (command.startsWith("#delete ")) {
                new DeleteCommandHandler().handleCommand(command, client);
            }
            else {
                String msg = JSONMessageComposer.chatMessage(command);
                client.sendMessage(msg);
            }
        } catch (InvalidCommandException e) {
            CommonUtil.printPrompt();
        }

    }
}
