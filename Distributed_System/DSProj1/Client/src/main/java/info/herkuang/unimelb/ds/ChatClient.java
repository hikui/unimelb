package info.herkuang.unimelb.ds;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.messageHandler.RoomChangeMessageHandler;
import info.herkuang.unimelb.ds.messageHandler.RoomContentsMessageHandler;
import info.herkuang.unimelb.ds.messageHandler.RoomListMessageHandler;
import info.herkuang.unimelb.ds.util.CommonUtil;
import info.herkuang.unimelb.ds.util.JSONMessageComposer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class ChatClient {

    private String serverHost;
    private int serverPort;

    private BufferedReader reader;
    private PrintWriter writer;
    private Socket socket;
    private Thread receiverThread;

    private Logger logger = LogManager.getLogger(this.getClass());

    public ChatClient(String serverHost, int serverPort) {
        this.serverHost = serverHost;
        this.serverPort = serverPort;
    }

    public void start() {
        try {
            Socket socket = new Socket(serverHost, serverPort);
            this.socket = socket;
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
            this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));

            // Initialise
            handleInitialiseMessages();

            Scanner sc = new Scanner(System.in);

            MessageDispatcher msgDispatcher = new MessageDispatcher(socket);

            Thread t = new Thread(msgDispatcher);
            receiverThread = t;
            t.start();

            while (true) {
                String line = sc.nextLine();
                if (line.equals("#quit")) {
                    break;
                }
                CommandDispatcher.dispatch(line, this);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            cleanup();
        }

    }

    public void sendMessage(String message) {
        this.writer.println(message);
        this.writer.flush();
    }

    public void cleanup() {
        // send quit message to the server and wait for receive.
        String msg = JSONMessageComposer.quit();
        this.sendMessage(msg);
        receiverThread.interrupt();
        try {
            // wait for the receiver thread
            receiverThread.join(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Disconnected from " + serverHost);
        }
    }

    public void handleInitialiseMessages() {
        // There must be 4 messages from the server at the beginning.
        try {
            String identityJson = reader.readLine();
            String roomChangeJson = reader.readLine();
            String roomContentsJson = reader.readLine();
            String roomListJson = reader.readLine();

            logger.debug(identityJson);
            logger.debug(roomChangeJson);
            logger.debug(roomListJson);
            logger.debug(roomContentsJson);

            JSONObject identityObj = (JSONObject) JSONValue.parse(identityJson);
            JSONObject roomChangeObj = (JSONObject) JSONValue.parse(roomChangeJson);

            UserInfo.userId = (String) identityObj.get("identity");
            UserInfo.roomId = (String) roomChangeObj.get("roomid");

            System.out.println("Connected to " + serverHost + " as " + UserInfo.userId);
            new RoomListMessageHandler().handleMessage(roomListJson);
            new RoomChangeMessageHandler().handleMessage(roomChangeJson);
            new RoomContentsMessageHandler().handleMessage(roomContentsJson);
            CommonUtil.printPrompt();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.err.println("Failed to get init message");
            System.exit(1);
        } catch (InvalidMessageFormatException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Arguments arguments = new Arguments();
        CmdLineParser parser = new CmdLineParser(arguments);
        try {
            parser.parseArgument(args);
            ChatClient c = new ChatClient(arguments.remoteServer, arguments.port);
            c.start();
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
        }
    }
}
