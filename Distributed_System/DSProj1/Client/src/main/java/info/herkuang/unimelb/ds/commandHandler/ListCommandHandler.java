package info.herkuang.unimelb.ds.commandHandler;

import info.herkuang.unimelb.ds.ChatClient;
import info.herkuang.unimelb.ds.exceptions.InvalidCommandException;
import info.herkuang.unimelb.ds.util.JSONMessageComposer;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class ListCommandHandler implements CommandHandler {
    @Override
    public void handleCommand(String command, ChatClient client) throws InvalidCommandException {
        String msg = JSONMessageComposer.list();
        client.sendMessage(msg);
    }
}
