package info.herkuang.unimelb.ds;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.messageHandler.*;
import info.herkuang.unimelb.ds.util.CommonUtil;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class MessageDispatcher implements Runnable {

    private Socket socket;
    private BufferedReader reader;

    public MessageDispatcher(Socket socket) {
        this.socket = socket;
        try {
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String inputLine;
        try {
            while ((inputLine = this.reader.readLine()) != null) {
                JSONObject obj = (JSONObject) JSONValue.parse(inputLine);
                if (Thread.interrupted()) {
                    // The last message from the server before disconnection.
                    // Only RoomChange message is allowed
                    if (obj == null) return;
                    String type = (String)obj.get("type");
                    if (type.equals("roomchange")) {
                        new RoomChangeMessageHandler().handleMessage(inputLine);
                    }
                    return;
                }
                if (obj == null) continue;
                String type = (String)obj.get("type");
                switch (type) {
                    case "message":
                        new ChatMessageHandler().handleMessage(inputLine);
                        break;
                    case "newidentity":
                        new NewIdentityMessageHandler().handleMessage(inputLine);
                        break;
                    case "roomcontents":
                        new RoomContentsMessageHandler().handleMessage(inputLine);
                        break;
                    case "roomlist":
                        new RoomListMessageHandler().handleMessage(inputLine);
                        break;
                    case "error":
                        new ErrorMessageHandler().handleMessage(inputLine);
                        break;
                    case "roomchange":
                        new RoomChangeMessageHandler().handleMessage(inputLine);
                        break;
                    default:
                        continue;
                }
                CommonUtil.printPrompt();
            }
            if (!Thread.interrupted()) {
                System.out.println("Server is dead");
                System.exit(1);
            }
        } catch (IOException e) {
            // Do nothing
        } catch (InvalidMessageFormatException e) {
            // Do nothing
        }
    }
}
