package info.herkuang.unimelb.ds.messageHandler;

import info.herkuang.unimelb.ds.UserInfo;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class RoomChangeMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String identity = (String) obj.get("identity");
        String former = (String) obj.get("former");
        String roomid = (String) obj.get("roomid");

        if(roomid == null || roomid.equals("")) {
            // null indicates that the socket is going to close, ignore everything.
            System.out.println(identity + " leaves " + former);
            return;
        }

        if (roomid.equals(former)) {
            // failed to move
            System.out.println("Failed to join room");
            return;
        }

        if (former == null) {
            System.out.println(identity + " moves to " + roomid);
        } else {
            System.out.println(identity + " moved from " + former + " to " + roomid);
        }

        if (UserInfo.userId.equals(identity)) {
            UserInfo.roomId = roomid;
        }
    }
}
