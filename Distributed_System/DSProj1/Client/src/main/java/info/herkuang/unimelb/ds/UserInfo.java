package info.herkuang.unimelb.ds;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Used to store the user information
 * Created by Heguang Miao on 12/09/2015.
 */
public class UserInfo {
    public static String userId;
    public static String roomId;
    public static Set<String> ownedRooms = new HashSet<>();
}
