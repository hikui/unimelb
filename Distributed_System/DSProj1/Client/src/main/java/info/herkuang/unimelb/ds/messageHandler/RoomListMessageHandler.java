package info.herkuang.unimelb.ds.messageHandler;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class RoomListMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        JSONArray rooms = (JSONArray)obj.get("rooms");
        for (Object o : rooms) {
            JSONObject roomInfo = (JSONObject)o;
            String roomId = (String) roomInfo.get("roomid");
            Long count = (Long) roomInfo.get("count");
            System.out.println(roomId + ": " + count + " guests");
        }
    }
}
