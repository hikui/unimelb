package info.herkuang.unimelb.ds.messageHandler;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public interface MessageHandler {
    void handleMessage(String message) throws InvalidMessageFormatException;
}
