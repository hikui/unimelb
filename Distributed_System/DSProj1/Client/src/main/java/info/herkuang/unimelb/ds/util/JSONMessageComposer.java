package info.herkuang.unimelb.ds.util;

import org.json.simple.JSONObject;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class JSONMessageComposer {

    public static String chatMessage(String message) {
        JSONObject obj = new JSONObject();
        obj.put("type", "message");
        obj.put("content", message);
        return obj.toJSONString();
    }

    public static String identityChange(String newIdentity) {
        JSONObject obj = new JSONObject();
        obj.put("type", "identitychange");
        obj.put("identity", newIdentity);
        return obj.toJSONString();
    }

    public static String join(String room) {
        JSONObject obj = new JSONObject();
        obj.put("type", "join");
        obj.put("roomid", room);
        return obj.toString();
    }

    public static String who(String room) {
        JSONObject obj = new JSONObject();
        obj.put("type", "who");
        obj.put("roomid", room);
        return obj.toString();
    }

    public static String list() {
        JSONObject obj = new JSONObject();
        obj.put("type", "list");
        return obj.toString();
    }

    public static String quit() {
        JSONObject obj = new JSONObject();
        obj.put("type", "quit");
        return obj.toString();
    }

    public static String createRoom(String roomId) {
        JSONObject obj = new JSONObject();
        obj.put("type", "createroom");
        obj.put("roomid", roomId);
        return obj.toString();
    }

    public static String kick(String roomId, Integer time, String identity) {
        JSONObject obj = new JSONObject();
        obj.put("type", "kick");
        obj.put("time", time);
        obj.put("identity", identity);
        obj.put("roomid", roomId);
        return obj.toString();
    }

    public static String delete(String roomId) {
        JSONObject obj = new JSONObject();
        obj.put("type", "delete");
        obj.put("roomid", roomId);
        return obj.toString();
    }
}
