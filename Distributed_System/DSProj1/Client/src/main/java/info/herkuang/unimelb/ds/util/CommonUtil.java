package info.herkuang.unimelb.ds.util;

import info.herkuang.unimelb.ds.UserInfo;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class CommonUtil {
    public static void printPrompt(){
        System.out.print("[" + UserInfo.roomId + "] " + UserInfo.userId + "> ");
    }
}
