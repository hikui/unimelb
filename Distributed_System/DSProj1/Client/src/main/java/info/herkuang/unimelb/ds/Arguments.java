package info.herkuang.unimelb.ds;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class Arguments {

    @Argument
    public String remoteServer;
    @Option(name="-p", usage = "Port")
    public int port = 4444;
}
