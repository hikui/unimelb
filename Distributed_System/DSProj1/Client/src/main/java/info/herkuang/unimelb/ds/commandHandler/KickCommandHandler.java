package info.herkuang.unimelb.ds.commandHandler;

import info.herkuang.unimelb.ds.ChatClient;
import info.herkuang.unimelb.ds.exceptions.InvalidCommandException;
import info.herkuang.unimelb.ds.util.JSONMessageComposer;

/**
 * Created by Heguang Miao on 13/09/2015.
 */
public class KickCommandHandler implements CommandHandler{

    @Override
    public void handleCommand(String command, ChatClient client) throws InvalidCommandException {
        String[] cmd = command.split(" ");
        if (cmd.length < 4) {
            throw new InvalidCommandException();
        }
        String roomId = cmd[1];
        Integer time = new Integer(cmd[2]);
        String identity = cmd[3];
        client.sendMessage(JSONMessageComposer.kick(roomId, time, identity));
    }
}
