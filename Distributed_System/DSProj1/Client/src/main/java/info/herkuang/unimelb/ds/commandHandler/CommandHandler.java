package info.herkuang.unimelb.ds.commandHandler;

import info.herkuang.unimelb.ds.ChatClient;
import info.herkuang.unimelb.ds.exceptions.InvalidCommandException;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public interface CommandHandler {
    void handleCommand(String command, ChatClient client) throws InvalidCommandException;
}
