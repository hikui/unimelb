package info.herkuang.unimelb.ds.messageHandler;

import info.herkuang.unimelb.ds.UserInfo;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class NewIdentityMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String former = (String)obj.get("former");
        String newId = (String)obj.get("identity");
        if (UserInfo.userId.equals(former)) {
            UserInfo.userId = newId;
        }
        if (former.equals(newId)) {
            System.out.println("Requested identity invalid or in use");
        }else {
            System.out.println(former + " is now " + newId);
        }
    }
}
