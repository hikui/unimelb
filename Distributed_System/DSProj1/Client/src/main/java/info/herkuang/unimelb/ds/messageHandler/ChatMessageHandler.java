package info.herkuang.unimelb.ds.messageHandler;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class ChatMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String identity = (String)obj.get("identity");
        String content = (String)obj.get("content");
        System.out.println(identity + ": " + content);
    }
}
