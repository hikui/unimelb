package info.herkuang.unimelb.ds.utils;

/**
 * Created by hikui on 11/09/2015.
 */
public interface NotificationObserver {
    void didReceiveNotification(String actionName, Object userData);
}