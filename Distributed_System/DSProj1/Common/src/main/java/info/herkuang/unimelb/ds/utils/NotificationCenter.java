package info.herkuang.unimelb.ds.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Notification center is a tool to implement the observer pattern
 * Created by hikui on 11/09/2015.
 */
public class NotificationCenter {
    public static NotificationCenter defaultNotificationCenter = new NotificationCenter("Default");

    public String name;

    private Map<String, Set<NotificationObserver>> nameObserverMap = new HashMap<>();

    public NotificationCenter(String name) {
        this.name = name;
    }

    public synchronized void addObserver(NotificationObserver observer, String actionName) {
        Set<NotificationObserver> observerSet = nameObserverMap.get(actionName);
        if (observerSet == null) {
            observerSet = new HashSet<>();
            observerSet.add(observer);
            nameObserverMap.put(actionName, observerSet);
        }
        observerSet.add(observer);
    }

    public synchronized void removeObserver(NotificationObserver observer, String actionName) {
        Set observerSet = nameObserverMap.get(actionName);
        if (observerSet == null) {
            return;
        }
        observerSet.remove(observer);
    }

    public synchronized void removeObserver(NotificationObserver observer) {

        for (String key : nameObserverMap.keySet()){
            removeObserver(observer, key);
        }
    }

    public synchronized void postNotification(String actionName, Object userData) {
        Set<NotificationObserver> observerSet = nameObserverMap.get(actionName);
        for (NotificationObserver observer : observerSet) {
            observer.didReceiveNotification(actionName, userData);
        }
    }

}
