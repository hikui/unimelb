package info.herkuang.unimelb.ds.utils;

/**
 * Created by Heguang Miao on 13/09/2015.
 */
public class Pair<A, B> {
    public A fst;
    public B snd;
    public Pair(A fst, B snd) {
        this.fst = fst;
        this.snd = snd;
    }
}
