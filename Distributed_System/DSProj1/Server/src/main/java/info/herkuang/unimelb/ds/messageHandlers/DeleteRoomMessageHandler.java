package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.models.ChatRoom;
import info.herkuang.unimelb.ds.models.User;
import info.herkuang.unimelb.ds.services.RoomService;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Heguang Miao on 13/09/2015.
 */
public class DeleteRoomMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String roomId = (String) obj.get("roomid");
        ChatRoom r = RoomService.findRoom(roomId);
        Set<User> userListCopy = null;
        if (r != null) {
            userListCopy = new HashSet<>(r.userList);
        }
        boolean success = RoomService.deleteRoom(r, worker.user);
        if (success) {
            for(User u : userListCopy) {
                String rmch = JSONMessageCreator.roomChangeMessage(r, RoomService.MainHall, u);
//                r.broadcast(rmch); // included in the MainHall
                RoomService.MainHall.broadcast(rmch);
            }
            String rmlst = JSONMessageCreator.roomListMessage(RoomService.getRoomList());
            for (User u : userListCopy){
                u.associatedWorker.sendMessage(rmlst);
            }
        } else {
            String errMsg = JSONMessageCreator.errorMessage("Deleting room failed");
            worker.sendMessage(errMsg);
        }
    }
}
