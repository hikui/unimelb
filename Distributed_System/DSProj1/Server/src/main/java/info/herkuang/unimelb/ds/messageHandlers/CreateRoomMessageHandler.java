package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.CreateRoomFailedException;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.models.ChatRoom;
import info.herkuang.unimelb.ds.services.RoomService;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class CreateRoomMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String roomId = (String)obj.get("roomid");
        try {
            RoomService.createRoom(roomId, worker.user);
            worker.sendMessage(JSONMessageCreator.roomListMessage(RoomService.getRoomList()));
        } catch (CreateRoomFailedException e) {
            worker.sendMessage(JSONMessageCreator.errorMessage("Room" + roomId + " is invalid or already in use."));
        }

    }
}
