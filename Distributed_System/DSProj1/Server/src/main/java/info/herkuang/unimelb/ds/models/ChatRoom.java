package info.herkuang.unimelb.ds.models;

import info.herkuang.unimelb.ds.services.RoomService;
import info.herkuang.unimelb.ds.utils.NotificationCenter;
import info.herkuang.unimelb.ds.utils.NotificationObserver;
import info.herkuang.unimelb.ds.utils.Pair;

import java.util.*;

/**
 * Created by Heguang Miao on 4/09/2015.
 */
public class ChatRoom implements NotificationObserver {

    public String roomId;
    public User owner;
    public Set<User> userList = new HashSet<>();

    public Map<User, Pair<Date, Integer>> blackList = new HashMap<>();

    public ChatRoom() {
        NotificationCenter.defaultNotificationCenter.addObserver(this, "UserLogout");
    }

    public ChatRoom(String roomId) {
        this();
        this.roomId = roomId;
    }

    /**
     * Do some cleanup when a user logged out
     * @param actionName
     * @param user
     */
    @Override
    public void didReceiveNotification(String actionName, Object user) {
        if (actionName.equals("UserLogout")) {
            userList.remove(user);
            blackList.remove(user);
            if (owner == user) {
                owner = null;
            }
            if (owner == null && userList.size() == 0) {
                RoomService.deleteRoom(this, null);
            }
        }
    }

    public boolean canJoin(User user) {
        Pair<Date, Integer> datePair = blackList.get(user);
        if (datePair == null) return true;
        Date kickOffTime = datePair.fst;
        Integer timeInterval = datePair.snd;
        Date now = new Date();
        if ((now.getTime() - kickOffTime.getTime()) > (long)timeInterval * 1000) {
            blackList.remove(user);
            return true;
        }
        return false;
    }

    public void broadcast(String msg) {
        for(User u : userList) {
            u.associatedWorker.sendMessage(msg);
        }
    }

    @Override
    public String toString() {
        return this.roomId;
    }

    /**
     * The room service should call this on deletion in case of memory leak.
     */
    public void cleanup() {
        NotificationCenter.defaultNotificationCenter.removeObserver(this);
    }
}
