package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.models.User;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;

/**
 * Created by Heguang Miao on 13/09/2015.
 */
public class QuitMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException {
        // 1. send RoomChange message to all clients in that room.
        String msg = JSONMessageCreator.roomChangeMessage(worker.user.currentRoom, null, worker.user);
        worker.user.currentRoom.broadcast(msg);
        // 2. if the user is the owner of some rooms, set the owner to null
        // this is done in UserService.destroyUser() and the NotificationCenter
    }
}
