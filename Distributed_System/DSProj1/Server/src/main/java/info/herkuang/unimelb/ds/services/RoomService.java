package info.herkuang.unimelb.ds.services;

import info.herkuang.unimelb.ds.exceptions.CreateRoomFailedException;
import info.herkuang.unimelb.ds.exceptions.RefusedToJoinException;
import info.herkuang.unimelb.ds.models.ChatRoom;
import info.herkuang.unimelb.ds.models.User;
import info.herkuang.unimelb.ds.utils.Pair;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hikui on 8/09/2015.
 */
public class RoomService {
    private static Set<ChatRoom> rooms = new HashSet<>();

    public static ChatRoom MainHall = new ChatRoom("MainHall");


    public static Set<ChatRoom> getRoomList() {
        Set<ChatRoom> tmp = new HashSet<>(rooms);
        tmp.add(MainHall);
        return tmp;
    }

    public static synchronized ChatRoom createRoom(String roomId, User owner) throws CreateRoomFailedException {
        String pattern = "^[a-zA-Z]\\w{2,31}$";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(roomId);
        if (!m.matches()) {
            throw new CreateRoomFailedException();
        }

        ChatRoom tmp = null;
        for (ChatRoom aRoom : rooms) {
            if (aRoom.roomId.equals(roomId)) {
                tmp = aRoom;
                break;
            }
        }
        if(tmp!=null) {
            throw new CreateRoomFailedException();
        }
        ChatRoom room = new ChatRoom(roomId);
        room.owner = owner;
        rooms.add(room);
        return room;
    }


    public static synchronized void joinRoom(ChatRoom room, User user) throws RefusedToJoinException {
        if(room == null || user == null) {
            throw new RefusedToJoinException();
        }

        if (room == user.currentRoom) {
            throw new RefusedToJoinException();
        }

        if (! room.canJoin(user)) {
            throw new RefusedToJoinException();
        }
        room.userList.add(user);
        user.currentRoom = room;
    }


    public static synchronized void quitRoom(ChatRoom room, User user) {
        if (room == null || user == null) return;
        room.userList.remove(user);
        if (user.currentRoom == room) {
            user.currentRoom = MainHall;
        }
        // if the room doesn't have an owner and it's empty, delete the room.
        if (room != MainHall && room.owner == null && room.userList.size() == 0) {
            deleteRoom(room, null);
        }
    }

    public static synchronized boolean deleteRoom(ChatRoom room, User user) {
        if (room == null || room.owner != user || room == MainHall) {
            return false;
        }
        Set<User> userListCopy = new HashSet<>(room.userList);
        for (User u : userListCopy) {
            try {
                moveRoom(room, MainHall, u);
            } catch (RefusedToJoinException e) {
                e.printStackTrace();
            }
        }
        rooms.remove(room);
        room.cleanup();
        return true;
    }

    public static synchronized void moveRoom(ChatRoom from, ChatRoom to, User user) throws RefusedToJoinException {
        if (to == null || user == null) throw new RefusedToJoinException();
        joinRoom(to, user); // join first, if failed to join, the exception will be thrown.
        quitRoom(from, user);
    }

    public static ChatRoom findRoom(String roomId) {
        if (roomId == null) return null;
        for (ChatRoom r : getRoomList()){
            if (r.roomId.equals(roomId)) {
                return r;
            }
        }
        return null;
    }

    public static synchronized boolean kickUser(ChatRoom room, User commander, User user, Integer timeInterval) {
        if (commander == user) {
            // A user should not kick himself.
            return false;
        }
        if (room.owner != commander) {
            return false;
        }
        if (!room.userList.contains(user)) {
            return false;
        }
        // 1. add to the black list
        Date now = new Date();
        Pair<Date, Integer> itm = new Pair<>(now, timeInterval);
        room.blackList.put(user, itm);
        // 2. move user to the main hall
        try {
            moveRoom(room, MainHall, user);
        } catch (RefusedToJoinException e) {
            // Do nothing
        }
        return true;
    }
}
