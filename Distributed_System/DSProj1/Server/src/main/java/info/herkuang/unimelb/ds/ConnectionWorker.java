package info.herkuang.unimelb.ds;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.exceptions.RefusedToJoinException;
import info.herkuang.unimelb.ds.messageHandlers.QuitMessageHandler;
import info.herkuang.unimelb.ds.models.User;
import info.herkuang.unimelb.ds.services.RoomService;
import info.herkuang.unimelb.ds.services.UserService;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.*;
import java.net.Socket;

/**
 * Created by hikui on 4/09/2015.
 */
public class ConnectionWorker implements Runnable {

    private Socket socket;
//    private InputStream input
//    private OutputStream output

    private BufferedReader reader;
    private PrintWriter writer;

    private Logger logger = org.apache.logging.log4j.LogManager.getLogger(this.getClass());

    public User user; //user for current connection

    public ConnectionWorker(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            initialise();
            String inputLine;
            while ((inputLine = this.reader.readLine()) != null) {
                logger.debug(this.socket.getInetAddress().toString() + " says: " + inputLine);
                JSONObject obj = (JSONObject) JSONValue.parse(inputLine);
                if (obj != null) {
                    String type = (String)obj.get("type");
                    if (type.equals("quit")){
                        break;
                    }
                }
                MessageDispatcher.sharedDispatcher.dispatch(this, inputLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logger.debug(this.socket.getInetAddress() + " is going to the connection");
            try {
                cleanup();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // TODO: test exception
    }

    private void initialise() throws IOException {


        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
        this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));

        // create associated user
        this.user = UserService.createUser();
        logger.debug(this.socket.getInetAddress() + " created user: " + this.user.userId);
        user.associatedWorker = this;
        JSONObject rmChangeMsg = null;
        try {
            RoomService.moveRoom(null, RoomService.MainHall, user);
            ConnectionWorkerManager.registerWorker(this);
            printWelcomeInfo();
        } catch (RefusedToJoinException e) {
            e.printStackTrace();
        }
    }

    private void printWelcomeInfo() {
        sendMessage(JSONMessageCreator.newIdentityMessage(null, user.userId));
        sendMessage(JSONMessageCreator.roomChangeMessage(null, RoomService.MainHall, user));
        sendMessage(JSONMessageCreator.roomContentsMessage(RoomService.MainHall));
        sendMessage(JSONMessageCreator.roomListMessage(RoomService.getRoomList()));
    }

    private void cleanup() throws IOException {
        try {
            new QuitMessageHandler().handleMessage(null, this);
        } catch (InvalidMessageFormatException e) {
            e.printStackTrace();
        }

        this.reader.close();
        this.writer.close();
        this.socket.close();
        UserService.destroyUser(this.user);
        ConnectionWorkerManager.unregisterWorker(this);
    }

    public void sendMessage(String message) {
        logger.debug("Server sends: " + message + " to " + this.socket.getInetAddress());
        this.writer.println(message);
        this.writer.flush();
    }
}
