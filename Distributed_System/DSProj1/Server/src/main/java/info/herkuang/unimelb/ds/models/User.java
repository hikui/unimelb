package info.herkuang.unimelb.ds.models;

import info.herkuang.unimelb.ds.ConnectionWorker;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by hikui on 4/09/2015.
 */
public class User {

    public String userId;

    public ConnectionWorker associatedWorker;

    public ChatRoom currentRoom;

    public User(String id) {
        this.userId = id;
    }

    @Override
    public String toString() {
        return this.userId;
    }
}
