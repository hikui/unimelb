package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.exceptions.RefusedToJoinException;
import info.herkuang.unimelb.ds.models.ChatRoom;
import info.herkuang.unimelb.ds.services.RoomService;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class JoinMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String roomId = (String)obj.get("roomid");
        if (roomId == null) {
            throw new InvalidMessageFormatException();
        }
        ChatRoom from = worker.user.currentRoom;
        ChatRoom to = RoomService.findRoom(roomId);
        try {
            RoomService.moveRoom(from, to, worker.user);
        } catch (RefusedToJoinException e) {
            to = from;
        }

        String msg = JSONMessageCreator.roomChangeMessage(from, to, worker.user);
        if (to == from) {
            // did not change, only send to the client requested.
            worker.sendMessage(msg);
        } else {
            // 1. send Room Change message to to clients currently in the
            // requesting client's current room and the requesting client's requested room
            from.broadcast(msg);
            to.broadcast(msg);
            // 2. If client is changing to the MainHall then the server will also send a RoomContents message to the
            // client (for the MainHall) and a RoomList message after the RoomChange message.
            if (to == RoomService.MainHall) {
                worker.sendMessage(JSONMessageCreator.roomContentsMessage(RoomService.MainHall));
                worker.sendMessage(JSONMessageCreator.roomListMessage(RoomService.getRoomList()));
            }
        }
    }


}
