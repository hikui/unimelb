package info.herkuang.unimelb.ds.utils;

import info.herkuang.unimelb.ds.models.ChatRoom;
import info.herkuang.unimelb.ds.models.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Set;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class JSONMessageCreator {

    public static String roomChangeMessage(ChatRoom from, ChatRoom to, User user) {
        JSONObject obj = new JSONObject();
        obj.put("type","roomchange");
        obj.put("identity", user.toString());
        if (from != null) {
            obj.put("former", from.toString());
        }

        if (to != null) {
            obj.put("roomid", to.toString());
        } else {
            obj.put("roomid", "");
        }
        return obj.toJSONString();
    }

    public static String roomListMessage(Set<ChatRoom> rooms) {
        JSONObject rmListMsg = new JSONObject();
        rmListMsg.put("type","roomlist");
        JSONArray roomsArr = new JSONArray();
        for (ChatRoom it : rooms) {
            JSONObject rm = new JSONObject();
            rm.put("roomid",it.roomId);
            rm.put("count", it.userList.size());
            roomsArr.add(rm);
        }
        rmListMsg.put("rooms", roomsArr);
        return rmListMsg.toJSONString();
    }

    public static String roomContentsMessage(ChatRoom room) {
        JSONObject obj = new JSONObject();
        obj.put("type", "roomcontents");
        obj.put("roomid", room.roomId);
        obj.put("owner", room.owner == null? null : room.owner.userId);
        JSONArray identities = new JSONArray();
        for (User u : room.userList) {
            identities.add(u.userId);
        }

        obj.put("identities", identities);
        return obj.toJSONString();
    }

    public static String newIdentityMessage(String former, String identity) {
        JSONObject newIdentityObj = new JSONObject();
        newIdentityObj.put("type","newidentity");
        newIdentityObj.put("former", former);
        newIdentityObj.put("identity", identity);
        return newIdentityObj.toJSONString();
    }

    public static String errorMessage(String msg) {
        JSONObject obj = new JSONObject();
        obj.put("type", "error");
        obj.put("msg",msg);
        return obj.toJSONString();
    }
}
