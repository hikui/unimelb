package info.herkuang.unimelb.ds;


import org.apache.logging.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by hikui on 4/09/2015.
 */
public class ChatServer {

    private Logger logger = org.apache.logging.log4j.LogManager.getLogger(this.getClass());

    private Integer port;

    public ChatServer(Integer port) {
        this.port = port;
    }

    public void start() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            InetAddress serverAdd = serverSocket.getInetAddress();
            logger.info("Serving on " + serverAdd + " port " + port);
            while (true) {
                Socket s = serverSocket.accept();
                logger.debug(s.getInetAddress().toString() + " connects to the server");
                new Thread(new ConnectionWorker(s)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Arguments arguments = new Arguments();
        CmdLineParser parser = new CmdLineParser(arguments);
        try {
            parser.parseArgument(args);
            new ChatServer(arguments.port).start();
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
        }
    }
}
