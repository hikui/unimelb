package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.models.ChatRoom;
import info.herkuang.unimelb.ds.models.User;
import info.herkuang.unimelb.ds.services.RoomService;
import info.herkuang.unimelb.ds.services.UserService;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 13/09/2015.
 */
public class KickMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String roomId = (String)obj.get("roomid");
        Long time = (Long)obj.get("time");
        String userId = (String)obj.get("identity");
        ChatRoom room = RoomService.findRoom(roomId);
        User userToKick = UserService.findUser(userId);
        boolean success = RoomService.kickUser(room, worker.user, userToKick, time.intValue());

        if (!success) {
            worker.sendMessage(JSONMessageCreator.errorMessage("Failed to kick user"));
        } else {
            // Same to JoinMessageHandler
            String msg = JSONMessageCreator.roomChangeMessage(room, RoomService.MainHall, userToKick);
            room.broadcast(msg);
            RoomService.MainHall.broadcast(msg);
            userToKick.associatedWorker.sendMessage(JSONMessageCreator.roomContentsMessage(RoomService.MainHall));
            userToKick.associatedWorker.sendMessage(JSONMessageCreator.roomListMessage(RoomService.getRoomList()));
        }
    }
}
