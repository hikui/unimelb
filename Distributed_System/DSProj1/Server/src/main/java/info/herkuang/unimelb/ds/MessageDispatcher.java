package info.herkuang.unimelb.ds;

import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.messageHandlers.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by hikui on 4/09/2015.
 */
public class MessageDispatcher {

    public static MessageDispatcher sharedDispatcher = new MessageDispatcher();



    private MessageDispatcher(){

    }

    public void dispatch(ConnectionWorker sender, String message) {
        JSONObject obj = (JSONObject)JSONValue.parse(message);
        if (obj == null) {
            return;
        }
        String type = (String)obj.get("type");
        try {
            switch (type) {
                case "identitychange":
                    new IdentityChangeMessageHandler().handleMessage(message, sender);
                    break;
                case "message":
                    new ChatMessageHander().handleMessage(message, sender);
                    break;
                case "who":
                    new WhoMessageHandler().handleMessage(message, sender);
                    break;
                case "list":
                    new ListMessageHandler().handleMessage(message, sender);
                    break;
                case "createroom":
                    new CreateRoomMessageHandler().handleMessage(message, sender);
                    break;
                case "join":
                    new JoinMessageHandler().handleMessage(message, sender);
                    break;
                case "kick":
                    new KickMessageHandler().handleMessage(message, sender);
                    break;
                case "delete":
                    new DeleteRoomMessageHandler().handleMessage(message, sender);
                    break;
                default:
                    sender.sendMessage(message);
                    break;
            }
        }catch (InvalidMessageFormatException e) {
            e.printStackTrace();
        }
    }
}
