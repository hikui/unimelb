package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.ConnectionWorkerManager;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.services.UserService;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class IdentityChangeMessageHandler implements MessageHandler{

    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException{
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String identity = (String)obj.get("identity");
        // validation
        if (identity == null) {
            throw new InvalidMessageFormatException();
        }

        String formerIdentity = worker.user.userId;

        boolean succeed = UserService.changeIdentity(worker.user, identity);
        if (!succeed) {
            identity = formerIdentity;
        }

        String newIdentityJSON = JSONMessageCreator.newIdentityMessage(formerIdentity, identity);

        if (!succeed) {
            // only send to the client that requested this message
            worker.sendMessage(newIdentityJSON);
        }else {
            // otherwise send to all clients
            ConnectionWorkerManager.broadcastMessage(newIdentityJSON);
        }


    }
}
