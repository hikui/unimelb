package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class ChatMessageHander implements MessageHandler {
    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException {
        JSONObject obj = (JSONObject) JSONValue.parse(message);
        if (obj == null) {
            throw new InvalidMessageFormatException();
        }
        String content = (String)obj.get("content");
        if (content == null) {
            throw new InvalidMessageFormatException();
        }
        JSONObject bcastObj = new JSONObject();
        bcastObj.put("type", "message");
        bcastObj.put("identity", worker.user.userId);
        bcastObj.put("content",content);
        worker.user.currentRoom.broadcast(bcastObj.toJSONString());
    }
}
