package info.herkuang.unimelb.ds;

import org.kohsuke.args4j.Option;

/**
 * Created by Heguang Miao on 13/09/2015.
 */
public class Arguments {
    @Option(name = "-p", usage = "Determine the port")
    public Integer port = 4444;
}
