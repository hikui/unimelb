package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;
import info.herkuang.unimelb.ds.services.RoomService;
import info.herkuang.unimelb.ds.utils.JSONMessageCreator;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public class ListMessageHandler implements MessageHandler {
    @Override
    public void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException {
        worker.sendMessage(JSONMessageCreator.roomListMessage(RoomService.getRoomList()));
    }
}
