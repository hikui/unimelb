package info.herkuang.unimelb.ds.messageHandlers;

import info.herkuang.unimelb.ds.ConnectionWorker;
import info.herkuang.unimelb.ds.exceptions.InvalidMessageFormatException;

/**
 * Created by Heguang Miao on 12/09/2015.
 */
public interface MessageHandler {
    void handleMessage(String message, ConnectionWorker worker) throws InvalidMessageFormatException;
}