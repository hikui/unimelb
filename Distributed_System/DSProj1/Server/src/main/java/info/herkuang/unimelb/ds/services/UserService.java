package info.herkuang.unimelb.ds.services;

import info.herkuang.unimelb.ds.models.User;
import info.herkuang.unimelb.ds.utils.NotificationCenter;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by hikui on 8/09/2015.
 */
public class UserService {
//    private static int userCount = 1

    private static Set<User> registeredUsers =  new HashSet<>();


    public static synchronized User createUser() {

        // find out the lowest unused int
        int lowestInt = 1;
        for (; ; lowestInt++) {
            String tmp = "guest" + lowestInt;
            User u = null;
            for (User aUser : registeredUsers) {
                if (aUser.userId.equals(tmp)) {
                    u = aUser;
                    break;
                }
            }
            if (u == null) {
                break;
            }
        }

        User u = new User("guest" + lowestInt);
        registeredUsers.add(u);

        return u;
    }

    public static synchronized boolean changeIdentity(User u, String identity) {
        if (u == null) return false;

        if (identity.length() < 3 || identity.length() > 16) {
            // invalid, do not change the identity.
            return false;
        }
        boolean alreadyInUse = false;
        for (User tmp : registeredUsers) {
            if (tmp.userId.equals(identity)) {
                alreadyInUse = true;
                break;
            }
        }
        if (!alreadyInUse) {
            u.userId = identity;
        }
        return !alreadyInUse;
    }

    public static synchronized void destroyUser(User u) {
        if (u == null) {
            return;
        }
        registeredUsers.remove(u);
        NotificationCenter.defaultNotificationCenter.postNotification("UserLogout",u);
    }

    public static User findUser(String userId) {
        for (User u : registeredUsers) {
            if (u.userId.equals(userId)) {
                return u;
            }
        }
        return null;
    }
}
