package info.herkuang.unimelb.ds;

import info.herkuang.unimelb.ds.models.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by hikui on 11/09/2015.
 */
public class ConnectionWorkerManager {
    private static Set<ConnectionWorker> workers = new HashSet<>();

    public static void registerWorker(ConnectionWorker worker) {
        workers.add(worker);
    }

    public static void unregisterWorker(ConnectionWorker worker) {
        workers.remove(worker);
    }

    public static void broadcastMessage(String message) {
        for (ConnectionWorker w : workers) {
            w.sendMessage(message);
        }
    }
}
