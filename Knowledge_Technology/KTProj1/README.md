## Introduction

This program is used to solve the problem of the Project 1 of COMP90049, and is written in Groovy.

## Requirements
* JDK 1.6 or higher
* Internet access

## Compile and Run
Use gradle to compile and run the program.

	cd /path/to/project
	./gradlew run [OPTIONS]
	
where options include:

	-q:	The path of the query file
	-d: The path of the tweet file
	-t: Number of worker threads
	-m: Method, either edit_distance or ngram
	-n: The N of N-grams
	--threshold: The dissimilarity threshold.
	
For example, if you want to run the program using the 3-grams with the dissimilarity threshold of 20 and 8 threads, the command should be:

	./gradlew run -q /path/to/queries -d /path/to/tweets -t 8 -m ngram -n 3 --threshold 20
	
If you want to run the program using the global edit distance with the same threshold and threads, the command should be:

	./gradlew run -q /path/to/queries -d /path/to/tweets -t 8 -m edit_distance --threshold 20
	
The output will be printed out directly. If you want to save results into a file, please use the redirection. For example:

	./gradlew run -q /path/to/queries -d /path/to/tweets -t 8 -m edit_distance --threshold 20 > feedback.txt
