package info.herkuang.kt

import org.kohsuke.args4j.Option

/**
 * Created by hikui on 26/08/2015.
 */
class Options {
    @Option(name='-t', usage = 'Sets the number of threads')
    Integer numThreads = 1

    @Option(name = '-q', usage = 'Sets the path of the query file')
    String queriesPath

    @Option(name = '-d', usage = 'Sets the path of the document')
    String docPath

    @Option(name = '-m', usage = 'Sets matching method, default is edit distance')
    String method

    @Option(name = '-n', usage = 'Sets N-gram')
    Integer ngram = 3

    @Option(name = '--threshold', usage = 'Sets dissimilarity threshold')
    Integer threshold = 20


}
