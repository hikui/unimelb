package info.herkuang.kt

/**
 * Created by hikui on 25/08/2015.
 */
class NGram {
    static int ngrams(int n, String s1, String s2) {
        def s1Ngram = ngramsOfString(n, s1)
        def s2Ngram = ngramsOfString(n, s2)
        def intersect = s1Ngram.intersect(s2Ngram)
        return s1Ngram.size() + s2Ngram.size() - 2 * intersect.size()
    }

    static int similarity(int n, String s1, String s2) {
        def s1Ngram = ngramsOfString(n, s1)
        def s2Ngram = ngramsOfString(n, s2)
        def intersect = s1Ngram.intersect(s2Ngram)
        return 2 * intersect.size() * 100 / (s1Ngram.size() + s2Ngram.size())
    }

    private static Set<String> ngramsOfString(int n, String s) {
        def set = new HashSet<String>()
        if (n > s.size()) {
            set.add(s)
            return set
        }

        (0..s.size() - n).each { int i ->
            set.add(s.subSequence(i,i+n))
        }

        return set
    }

    public static void main(String[] args) {
        println(similarity(3, 'PANORAMA', 'PAMORAMA'))
    }
}
