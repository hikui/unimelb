package info.herkuang.kt

import org.kohsuke.args4j.CmdLineException
import org.kohsuke.args4j.CmdLineParser


enum MatchingMethod {
    EditDistance,
    NGram
}

class Main {
    public static void main(String[] args) {

        Options bean = new Options()
        CmdLineParser parser = new CmdLineParser(bean)
        try {
            parser.parseArgument(args)
        } catch(CmdLineException e){
            System.err.println(e.getMessage())
            parser.printUsage(System.err)
            return
        }

        Integer numThreads = bean.numThreads
        String queryFilePath = bean.queriesPath
        String tweetFilePath = bean.docPath
        Integer threshold = bean.threshold
        String methodStr = bean.method
        Integer ngram = bean.ngram

        MatchingMethod method = null

        switch (methodStr) {
            case 'ngram':
                method = MatchingMethod.NGram
                break
            case 'edit_distance':
                method = MatchingMethod.EditDistance
                break
            default:
                method = MatchingMethod.EditDistance
        }

        def queries = new File(queryFilePath).readLines()

        def batch = queries.size() / numThreads

        def startTime = new Date()

        def threads = []
        (numThreads).times { Integer i ->
            def thread = Thread.start {
                int start = i * batch
                int end = i == numThreads? queries.size() : (i+1) * batch
                Main.match(method, queries,start, end, tweetFilePath, threshold, ngram)
            }
            threads.add(thread)
        }

        threads.each { Thread t ->
            t.join()
        }
        def endTime = new Date()
        printf('This task started at %s and ended at %s', startTime.toString(), endTime.toString())
//        match(queries,0,queries.size(),'/Users/hikui/Source/KT/tweets.3K.txt')
//        println(GlobalEditDistance.distance('a','b'))
    }

    def static match(MatchingMethod method,
                     List<String> queries,
                     int start,
                     int end,
                     String filepath,
                     int threshold,
                     int n = 3) {

        if (queries == null) {
            return
        }

        def tweetFile = new File(filepath)
        (start..end-1).each{ queryIdx ->
            def query = queries.get(queryIdx)
            query = query.toUpperCase()
            def arrq = query.split(' ')
            tweetFile.eachLine { tweet ->
                int tabPos = tweet.indexOf('\t')
                String tweetId = ''
                if(tabPos > 0) {
                    tweetId = tweet.substring(0,tabPos)
                    tweet = tweet.substring(tabPos+1)
                }
                tweet = tweet.trim()
                tweet = tweet.toUpperCase()
                def arrt = tweet.split(' ')
                if(arrt.length < arrq.length) {
                    return
                }
                def tokens = []
                for (int i = 0; i + arrq.length <= arrt.length; i++){
                    tokens.add(arrt[i..i+arrq.length-1])
                }
                boolean flag = false
                tokens.eachWithIndex { List<String> token, int i ->
                    if(flag){
                        return
                    }
                    def dissimilarity = []
                    arrq.eachWithIndex { String wq, int j ->
                        if (wq.length() == 0 || token[j].length() == 0) {
                            return
                        }
                        def distance = 0
                        if (method == MatchingMethod.EditDistance) {
                            distance = EditDistance.distance(wq, token[j]) * 100 / wq.length()
                        } else if (method == MatchingMethod.NGram) {
                            distance = 100 - NGram.similarity(n, wq, token[j])
                        }
                        dissimilarity.add(distance)
                    }
                    if (dissimilarity.size() == 0){
                        return
                    }

                    int overallDissimilarity = dissimilarity.sum() / dissimilarity.size()
                    if (overallDissimilarity < threshold){
                        println('##########')
                        println(tweetId + ' ' + tweet)
                        println(query)
                        println(token)
                        flag = true
                    }
                }
            }

        }
    }
}
